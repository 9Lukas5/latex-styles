# Make sure latex-styles is included to search path
ensure_path( 'TEXINPUTS', './latex-styles//' );

$out_dir = 'build';

add_cus_dep('sbl', 'sym', 0, 'makeglossaries');
add_cus_dep( 'acn', 'acr', 0, 'makeglossaries' );
add_cus_dep( 'glo', 'gls', 0, 'makeglossaries' );
sub makeglossaries {
    my ($base_name, $path) = fileparse( $_[0] );
    pushd $path;
    my $return = system "makeglossaries", $base_name;
    popd;
    return $return;
}

#`-file-line-error` is similar to `--interaction nonstopmode`, but shows the concrete line number
#Remove it, it you want pdflatex to stop on errors
$pdflatex = 'lualatex -shell-escape -file-line-error -interaction=nonstopmode -synctex=1 %O %S';

#SumatraPDF updates automatically
$preview_mode = 0;

#automatically call pdflatex (instead of latex)
$pdf_mode = 1;

# raise max repeats
$max_repeat = 10;

#remove more files than in the default configuration
@generated_exts = qw(acn acr alg aux bbl code fls glg glo gls idx ind ist lof lol lot nav out run.xml snm thm toc tpt);
