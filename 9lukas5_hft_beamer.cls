\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{9lukas5_hft_beamer}[2022/08/26 9Lukas5 hft beamer class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions
\LoadClass{beamer}

\RequirePackage{tikz}

\RequirePackage{fontspec}
\RequirePackage{textcomp}
\RequirePackage[utf8]{inputenc}
\RequirePackage{csquotes}

\RequirePackage{fancyhdr}

\RequirePackage{babel}
\useshorthands{"}
\addto\extrasenglish{\languageshorthands{ngerman}}
\addto\extrasamerican{\languageshorthands{ngerman}}
\addto\extrasbritish{\languageshorthands{ngerman}}

\RequirePackage{geometry}
\RequirePackage{calc}

\RequirePackage{caption}
\RequirePackage{xcolor}

\RequirePackage{nameref}
    \newcommand*{\currentname}{\@currentlabelname}

\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}

\newcommand{\pageofname}{of}
\newcommand{\slidename}{Slide}
\addto\captionsgerman{
    \renewcommand{\pageofname}{von}
    \renewcommand{\slidename}{Folie}
}

\def\theAuthor{\@author}
\def\theTitle{\@title}

\newcommand{\makePdfMetadata}{
    \hypersetup{
        pdfauthor={\theAuthor},
        pdftitle={\theTitle}
    }
}

\captionsetup{font={footnotesize,sf}}

% headings
\definecolor{sectionHeading}{RGB}{60,60,200}
\definecolor{HFTRED}{RGB}{200,60,60}

% table row colors
\definecolor{tableHeaderRow}{RGB}{110,110,200}
\definecolor{tableOddRow}{RGB}{230,230,255}
\definecolor{tableEvenRow}{RGB}{250,250,255}

% vertical space between elements
\let\olditem\item
\renewcommand{\item}{\olditem\vspace{1ex}}

\setbeamertemplate{bibliography item}[text]

\def\@hmargin{1cm}
    \setbeamersize{text margin left=\@hmargin}
    \setbeamersize{text margin right=\@hmargin}


\newcommand{\@myemptyvalue}{<empty>}
\newcommand{\@myempty}{\@myemptyvalue}

\newcommand{\@lastfootlinevalue}{\@myemptyvalue}
\newcommand{\@setlastfootlinevalue}[1]{\renewcommand{\@lastfootlinevalue}{#1}}

\newcommand{\@insertSemester}{\@myemptyvalue}
\newcommand{\@insertCourse}{\@myemptyvalue}
\newcommand{\@insertFootnoteTitle}{\@myemptyvalue}
\newcommand{\@insertLicense}{\@myemptyvalue}
\newcommand{\@insertAdditional}{\@myemptyvalue}
\newcommand{\@insertVersion}{\@myemptyvalue}

\newcommand{\semester}[1]{\renewcommand{\@insertSemester}{#1}}
\newcommand{\course}[1]{\renewcommand{\@insertCourse}{#1}}
\newcommand{\footnoteTitle}[1]{\renewcommand{\@insertFootnoteTitle}{#1}}
\newcommand{\license}[1]{\renewcommand{\@insertLicense}{#1}}
\newcommand{\additional}[1]{\renewcommand{\@insertAdditional}{#1}}
\newcommand{\version}[1]{\renewcommand{\@insertVersion}{#1}}

% Disable navigation symbols
\setbeamertemplate{navigation symbols}{}

\def\@headheight{1cm}
\setbeamertemplate{frametitle}{
    \begin{beamercolorbox}[wd=\paperwidth,ht=\@headheight]{frametitle}
        \begin{tikzpicture}
            \useasboundingbox(0,0) rectangle(\the\paperwidth,\@headheight);
            \fill[HFTRED] (0,0) rectangle(\the\paperwidth,0.3mm);
            \ifx\insertframesubtitle\@empty%
                {\node[anchor=west] at (\@hmargin,0.5\@headheight){\insertframetitle};}
                \else%
                {
                    \node[anchor=west] at (\@hmargin,0.6\@headheight){\insertframetitle};%
                    \node[anchor=west] at (\@hmargin,0.2\@headheight){\tiny\insertframesubtitle};%
                }
            \fi
        \end{tikzpicture}
    \end{beamercolorbox}
}

\newcommand{\@addNextFootlineElement}[1]{%
    \ifx#1\@myempty%
    \else%
        \ifx\@lastfootlinevalue\@myempty%
        \else%
            | %
        \fi%
        #1%
        \@setlastfootlinevalue{#1}%
    \fi
}

\setbeamertemplate{footline}{
    \begin{beamercolorbox}[wd=\paperwidth,ht=1.2cm]{footline}
        \begin{tikzpicture}
            \useasboundingbox(0,0) rectangle(\the\paperwidth,1.2cm);

            % Center Logo with hline
            \fill[HFTRED] (0,0.97cm) rectangle(\the\paperwidth,1cm);
            {\node[anchor=center] at (.5\paperwidth,1.15cm) {\color{HFTRED}Hochschule \textit{für Technik}};}
            {\node[anchor=center] at (.5\paperwidth,0.8cm) {\textbf{\color{HFTRED}Stuttgart}};}

            % Left side footer
            {\node[anchor=west] at (\@hmargin,0.5cm) {
                \@addNextFootlineElement{\@insertSemester}
                \@addNextFootlineElement{\@insertCourse}
                \@addNextFootlineElement{\@insertFootnoteTitle}
                \@addNextFootlineElement{\@insertAdditional}
            };}

            % Right side footer
            {\node[anchor=east] at (\paperwidth-\@hmargin,0.8cm) {\slidename\ \insertframenumber};}
            \@setlastfootlinevalue{\@myemptyvalue}
            {\node[anchor=east] at (\paperwidth-\@hmargin,0.5cm) {
                \color{gray}
                \@addNextFootlineElement{\@insertLicense}
                \@addNextFootlineElement{\@insertVersion}
            };}
        \end{tikzpicture}
    \end{beamercolorbox}
}
