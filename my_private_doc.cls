\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{my_private_doc}[2022/09/22 Lukas private documents class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

\RequirePackage{fontspec}
\RequirePackage{textcomp}
\RequirePackage[utf8]{inputenc}
\RequirePackage{csquotes}

\RequirePackage{fancyhdr}

\RequirePackage{babel}
\useshorthands{"}
\addto\extrasenglish{\languageshorthands{ngerman}}
\addto\extrasamerican{\languageshorthands{ngerman}}
\addto\extrasbritish{\languageshorthands{ngerman}}

\RequirePackage{geometry}

\RequirePackage{caption}
\RequirePackage{xcolor}
\RequirePackage{titlesec}

\RequirePackage{alphalph}
    \newcommand{\@AlphAlph}{}
    \let\@AlphAlph=\AlphAlph

\RequirePackage{setspace}

\setstretch{1.5}

\RequirePackage{enumitem}
    \setlist{noitemsep,topsep=0pt}

\RequirePackage{ifthen}

\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}

\newcommand{\pageOf}{of}
\addto\captionsgerman{
    \renewcommand{\pageOf}{von}
}

\def\theAuthor{\@author}
\def\theTitle{\@title}

\newcommand{\makePdfMetadata}{
    \hypersetup{
        pdfauthor={\theAuthor},
        pdftitle={\theTitle}
    }
}

\captionsetup{font={footnotesize,sf}}

% patching footnote command to be sffamily
\let\@oldfootnote\footnote
\renewcommand{\footnote}[1]{\@oldfootnote{\sffamily#1}}

\geometry
{
    a4paper,
    top=20mm,
    bottom=20mm,
    left=20mm,
    right=20mm,
    includehead,includefoot,
    headheight=27pt
}

% headings
\definecolor{sectionHeading}{RGB}{60,60,200}

% table row colors
\definecolor{tableHeaderRow}{RGB}{110,110,200}
\definecolor{tableOddRow}{RGB}{230,230,255}
\definecolor{tableEvenRow}{RGB}{250,250,255}

\newcommand{\tableHeaderCell}[1]{\cellcolor{tableHeaderRow}\textbf{\color{white}#1}}
\newcommand{\defaultTablePreamble}{%
    \sffamily%
    \rowcolors{0}{tableOddRow}{tableEvenRow}%
}
\newcolumntype{A}{>{\raggedright\arraybackslash}X}
\newcolumntype{B}{>{\centering\arraybackslash}X}
\newcolumntype{C}{>{\raggedleft\arraybackslash}X}
\newcolumntype{D}{>{\raggedright\arraybackslash}p}
\newcolumntype{E}{>{\centering\arraybackslash}p}
\newcolumntype{F}{>{\raggedleft\arraybackslash}p}

% Headings setup
\titleformat
{\section} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\LARGE} % format
{\thesection} %label
{1em} % separation
{} % before code
[\titlerule] % after code

\titleformat
{\subsection} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\Large} % format
{\thesubsection} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\subsubsection} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries\large} % format
{\thesubsubsection} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\paragraph} % command
%[] % shape
{\color{sectionHeading}\normalfont\bfseries} % format
{\theparagraph} %label
{1em} % separation
{} % before code
%[] % after code

\titleformat
{\subparagraph} % command
%[] % shape
{\color{sectionHeading}\normalfont\itshape} % format
{\theparagraph} %label
{1em} % separation
{} % before code
%[] % after code

\titlespacing*{\paragraph}
{0pt}
{10pt}
{1.5pt}

\titlespacing*{\subparagraph}
{0pt}
{10pt}
{1.5pt}

% footer
\newcommand{\@myemptyvalue}{<empty>}
\newcommand{\@myempty}{\@myemptyvalue}

\newcommand{\@lastfootlinevalue}{\@myemptyvalue}
\newcommand{\@setlastfootlinevalue}[1]{\renewcommand{\@lastfootlinevalue}{#1}}

\newcommand{\@insertFirstText}{\@myemptyvalue}
\newcommand{\@insertSecondText}{\@myemptyvalue}
\newcommand{\@insertFootnoteTitle}{\@myemptyvalue}
\newcommand{\@insertAdditional}{\@myemptyvalue}
\newcommand{\@insertLicense}{\@myemptyvalue}
\newcommand{\@insertVersion}{\@myemptyvalue}

\newcommand{\firstText}[1]{\renewcommand{\@insertFirstText}{#1}}
\newcommand{\secondText}[1]{\renewcommand{\@insertSecondText}{#1}}
\newcommand{\footnoteTitle}[1]{\renewcommand{\@insertFootnoteTitle}{#1}}
\newcommand{\additional}[1]{\renewcommand{\@insertAdditional}{#1}}
\newcommand{\license}[1]{\renewcommand{\@insertLicense}{#1}}
\newcommand{\version}[1]{\renewcommand{\@insertVersion}{#1}}

\newcommand{\@addNextFootlineElement}[1]{%
    \ifx#1\@myempty%
    \else%
        \ifx\@lastfootlinevalue\@myempty%
        \else%
            \ | %
        \fi%
        #1%
        \@setlastfootlinevalue{#1}%
    \fi
}

\newcommand{\frontmatter}[1]{
    \setMatter{Roman}{#1}
}

\newcommand{\mainmatter}[1]{
    \setMatter{arabic}{#1}
}

\newcommand{\backmatter}[1]{
    \setcounter{secnumdepth}{3}
    \setcounter{section}{0}
    \renewcommand\thesection{\Roman{section}}
    \renewcommand\thesubsection{\arabic{subsection}}
    \renewcommand\thesubsubsection{\arabic{subsection}.\arabic{subsubsection}}
    \setMatter{AlphAlph}{#1}
}

%front/main matter
\newcommand{\setMatter}[2]{
    \cleardoublepage
    \pagenumbering{#1}
    \fancypagestyle{parametrizedMatter}{
        \fancyhf{}

        \fancyhead[L]{\sffamily\textbf{Lukas Wiest}}
        \fancyhead[C]{\sffamily\small\theTitle}
        \fancyhead[R]{\sffamily\small\today}
        \renewcommand{\headrulewidth}{0.4pt}

        \fancyfoot[L]{%
            \sffamily%
            \small%
            \@addNextFootlineElement{\@insertFirstText}%
            \@addNextFootlineElement{\@insertSecondText}%
            \@addNextFootlineElement{\@insertFootnoteTitle}%
            \@addNextFootlineElement{\@insertAdditional}%
            \ifx\@lastfootlinevalue\@myempty%
            \else%
                \newline%
            \fi%
            \@setlastfootlinevalue{\@myemptyvalue}%
            \footnotesize\textcolor{gray}{%
                \@addNextFootlineElement{\@insertVersion}%
                \@addNextFootlineElement{\@insertLicense}%
            }%
        }
        \fancyfoot[R]{\sffamily\small \pagename{} \thepage{} \pageOf{} #2}
        \renewcommand{\footrulewidth}{0.4pt}
    }
    \pagestyle{parametrizedMatter}
}
